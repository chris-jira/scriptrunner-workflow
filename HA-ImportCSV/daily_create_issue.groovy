/*
Script: daily_create_issues.groovy
Purpose: This script serves the routine job - create issue from csv daily
Date: 22/03/2021
Version: 0.1
Provided By: TechNet
*/

import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.label.LabelManager

final projectKey = 'ISO'
final csvFile = "/mnt/jira/sharedhome/scripts/$projectKey/csv_issue_import/jira_import_test.csv"

// Get Jira Project ID
def project = ComponentAccessor.projectManager.getProjectObjByKey(projectKey)
assert project : "Could not find project with key $projectKey"

// Get CSV File Content
def csvContent = new File(csvFile).getText('UTF-8').replaceAll("\r\n", "\n").split("\n")

// Get Issue Fields From CSV Content
def fields = csvContent[0].split(',')

// Convert CSV Fields To Map and Trigger Create Issue Function
for (int i = 1; i < csvContent.length; i++) {
    def values = csvContent[i].split(",")
    def tmpMap = [:].withDefault { key -> return [] }
    for (int j = 0; j < fields.length; j++) {
        if (j < values.length && values[j] != "") {
            tmpMap.get(fields[j]).add(values[j])
        }
    }
	createIssue(project.id, tmpMap.get('Summary')[0], tmpMap.get('Issue Type')[0], tmpMap.get('Priority')[0], tmpMap.get('Assignee')[0], tmpMap.get('Component/s')[0], tmpMap.get('Labels'), tmpMap.get('Description')[0], tmpMap.get('status')[0], tmpMap.get('watcher'))   
}

def createIssue(projectId, summary, type, priority, assignee, component, labels, description, status, watchers) {
    def issueService = ComponentAccessor.issueService
	def constantsManager = ComponentAccessor.constantsManager
    def loggedInUser = ComponentAccessor.jiraAuthenticationContext.loggedInUser
    def projectComponentManager = ComponentAccessor.getProjectComponentManager()
	def labelManager = ComponentAccessor.getComponent(LabelManager)
    def watcherManager = ComponentAccessor.getWatcherManager()
    def workflowManager = ComponentAccessor.workflowManager
    
    def issueType = constantsManager.allIssueTypeObjects.findByName(type)
	assert issueType : "Could not find issue type with name $type"
 
	def issuePriority = constantsManager.priorities.findByName(priority)
    assert issuePriority : "Could not find priority with name $priorityName"
    
    def issueAssignee = ComponentAccessor.userManager.getUserByName(assignee)
    assert issueAssignee : "Could not find user with name $assignee"
    
    def issueComponent = projectComponentManager.findByComponentName(projectId, component)
    assert issueComponent : "Could not find component with name $component"
    
    def issueInputParameters = issueService.newIssueInputParameters().with {
    	setProjectId(projectId)
        setSummary(summary)
    	setIssueTypeId(issueType.id)
        setPriorityId(issuePriority.id)
        setAssigneeId(issueAssignee.name)
        setComponentIds(issueComponent.id)
        setDescription(description.substring( 1, description.length() - 1 ))
        setReporterId(loggedInUser.name)
	}
    
	def validationResult = issueService.validateCreate(loggedInUser, issueInputParameters)
	assert validationResult.isValid() : validationResult.errorCollection

	def result = issueService.create(loggedInUser, validationResult)
	assert result.isValid() : result.errorCollection
    
    def issue = result.getIssue()
    log.warn "======================"
    log.warn "Issue " + issue.id + " is created"
    
    for (label in labels) {
        labelManager.addLabel(loggedInUser, issue.id, label, false)
    }
    
    for (watcher in watchers) {
        def issueWatcher = ComponentAccessor.userManager.getUserByName(watcher)
        assert issueWatcher : "Could not find user with name $watcher"
        watcherManager.startWatching(issueWatcher, issue)
    }
    watcherManager.stopWatching(loggedInUser, issue);
    
	def workflowActionId = ""
    switch (status) {
        case "In Progress":
        	workflowActionId = workflowManager.getWorkflow(issue).allActions.findByName('Start Progress')?.id
        	log.warn "Move issue " + issue.id + " to In Progress"
        	break
        case "Done":
        	workflowActionId = workflowManager.getWorkflow(issue).allActions.findByName(status)?.id
        	log.warn "Move issue " + issue.id + " to Done"
        	break
        default:
            log.warn "No Transition is needed for issue " + issue.id
    }
    if (workflowActionId != "") {
        def transitionValidationResult = issueService.validateTransition(issueAssignee, issue.id, workflowActionId, issueInputParameters)
    	assert transitionValidationResult.isValid(): transitionValidationResult.errorCollection
    	def transitionResult = issueService.transition(loggedInUser, transitionValidationResult)
    	assert transitionResult.isValid(): transitionResult.errorCollection
    }
    log.warn "======================"
}
