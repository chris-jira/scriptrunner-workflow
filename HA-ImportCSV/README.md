# Background Information

HA wants to import Jira issues via CSV which contains “status” field and “watchers” field. When importing issues, it will assign watchers and move workflow status to destinations. 

## Difficulties

"status" and "watchers" are not available in create issue screen. However, import CSV function imples create screen to create issues. If those field cannot be provided on create screen, import CSV function will not work.

## Solutions

1. Create custom fields "status" & "watchers"
2. Assign custom fields to create screen
3. Setup workflow scriptrunner to create watchers and move status. 


## Solution 2
1. Go to Script Editor
2. Copy and paste daily_create_issue.groovy 

daily_create_issue.groovy is used to parse csv file and then create issue
