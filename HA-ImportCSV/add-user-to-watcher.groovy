import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.user.ApplicationUser

def customFieldManager = ComponentAccessor.getCustomFieldManager()
def watcherManager = ComponentAccessor.getWatcherManager()
def userUtil = ComponentAccessor.getUserUtil()

// Input Watcher Custom Field Name
final watcherCustomFieldName = "Watchers - Post Function"

// Get Watchers Custom Field By Field Name
def watcherCustomField = customFieldManager.getCustomFieldObjects(issue).findByName(watcherCustomFieldName)
assert watcherCustomField: "Could not find custom field with name $watcherCustomFieldName"

// Get watcher from field and then set watcher
def watchers = issue.getCustomFieldValue(watcherCustomField) as ArrayList<ApplicationUser>
for (watcher in watchers) {
    log.warn("Add user " + watcher + " to watcher")
    watcherManager.startWatching(watcher, issue)
}
