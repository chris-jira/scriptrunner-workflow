import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.json.JsonOutput
import groovy.transform.BaseScript
import groovy.json.JsonSlurper;
import groovy.json.StreamingJsonBuilder;
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response
  
@BaseScript CustomEndpointDelegate delegate
  
commitToGitLab(httpMethod: "GET") { MultivaluedMap queryParams ->
  
    def issueId = queryParams.getFirst("issueId") as String // use the issueId to retrieve this issue
      
    def flag = [
    type : 'success',
    title: "Build scheduled",
    close: 'auto',
    body : "A new build has been scheduled related with "+issueId
    ]
    
 
    URL url;
    def jobName = "Commit_Cucumber_To_GitLab"           // could come from a CF in the Test Plan
    def jenkinsHostPort = "10.9.17.77:8080"             // could be defined elsewhere
    def token = "iFBDOBhNhaxL4T9ass93HRXun2JF161Z"
    def username = "admin"                              // probably, would need to be stored elsewhere
    def password = "11ada92da71bcd4691cf4d6666a0b3c2ff"  
    def baseURL = "http://${jenkinsHostPort}/job/HA/job/${jobName}/buildWithParameters?token=${token}&issueKey=$issueId"
 
    url = new URL(baseURL);
    def body_req = []
 
    def authString = "${username}:${password}".bytes.encodeBase64().toString()
 
    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.setRequestMethod("POST");
    connection.setDoOutput(true)
    connection.doOutput = true
    connection.addRequestProperty("Authorization", "Basic ${authString}")
    connection.setRequestProperty("Content-Type", "application/json;charset=UTF-8")
    connection.outputStream.withWriter("UTF-8") { new StreamingJsonBuilder(it, body_req) }
    connection.connect();
    log.warn(connection.getResponseCode())
    log.warn(connection.getResponseMessage())
 
     
    if (connection.getResponseCode() == 201) {
        Response.ok(JsonOutput.toJson(flag)).build()
    } else {
        //Response.status(Response.Status.NOT_FOUND).entity("Problem scheduling job!").build();
    }
     
}
