## Introduction

User can click a button on issue page to commit the change of cucumber test field to Git repository.

## Workflow
1. User click a button (scriptrunner fragment) on test issue web page
2. Fragment (custom web item) triggers scriptrunner rest endpoint
3. Rest endpoint triggers Jenkins Build Job
4. Jenkins Xray plugin export cucumber feature file from Jira and commit to GitLab
